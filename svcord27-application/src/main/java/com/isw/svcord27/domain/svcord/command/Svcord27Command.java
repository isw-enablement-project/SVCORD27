package com.isw.svcord27.domain.svcord.command;

import org.springframework.stereotype.Service;
import com.isw.svcord27.sdk.domain.svcord.command.Svcord27CommandBase;
import com.isw.svcord27.sdk.domain.svcord.entity.Svcord27Entity;
import com.isw.svcord27.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord27.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord27.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord27.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord27.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord27.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord27Command extends Svcord27CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord27Command.class);

  public Svcord27Command(DomainEntityBuilder entityBuilder, Repository repo) {
    super(entityBuilder, repo);
  }

  @Override
  public com.isw.svcord27.sdk.domain.svcord.entity.Svcord27 createServicingOrderProducer(
      com.isw.svcord27.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput) {
    log.info("Svcord27Command.createServicingOrderProducer()");

    Svcord27Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord27().build();
    servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessStartDate(LocalDate.now());
    servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
    servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
    servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
    servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);

    ThirdPartyReferenceEntity thirdPartyReferenceEntity = new ThirdPartyReferenceEntity();
    thirdPartyReferenceEntity.setId("test1");
    thirdPartyReferenceEntity.setPassword("password");

    servicingOrderProcedure.setThirdPartyReference(thirdPartyReferenceEntity);

    return this.repo.getSvcord().getSvcord27().save(servicingOrderProcedure);
  }

  @Override
  public void updateServicingOrderProducer(com.isw.svcord27.sdk.domain.svcord.entity.Svcord27 instance,
      com.isw.svcord27.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput) {
    log.info("Svcord27Command.updateServicingOrderProducer()");

    Svcord27Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord27()
        .getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
    servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessEndDate(LocalDate.now());
    servicingOrderProcedure
        .setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
    servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

    log.info(updateServicingOrderProducerInput.getUpdateID().toString());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

    this.repo.getSvcord().getSvcord27().save(servicingOrderProcedure);
  }
}
