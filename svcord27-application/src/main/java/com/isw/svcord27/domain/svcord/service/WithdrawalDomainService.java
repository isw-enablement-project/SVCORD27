package com.isw.svcord27.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord27.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord27.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord27.sdk.domain.svcord.entity.Svcord27;
import com.isw.svcord27.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord27.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord27.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord27.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord27.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord27.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord27.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord27.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord27.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord27.domain.svcord.command.Svcord27Command;
import com.isw.svcord27.integration.partylife.service.RetrieveLogin;
import com.isw.svcord27.integration.paymord.service.PaymentOrder;
import com.isw.svcord27.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord27.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord27Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder, Repository repo) {
    super(entityBuilder, repo);
  }

  @NewSpan
  @Override
  public com.isw.svcord27.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(
      com.isw.svcord27.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput) {
    log.info("WithdrawalDomainService.execute()");

    // User validation Check
    RetrieveLoginInput loginInput = integrationEntityBuilder
        .getPartylife()
        .getRetrieveLoginInput()
        .setId("test1")
        .build();

    RetrieveLoginOutput loginOutput = retrieveLogin.execute(loginInput);
    if (loginOutput.getResult() != "SUCCESS") {
      return null;
    }

    // RootEntity servicing order 생성
    CustomerReferenceEntity customerReference = this.entityBuilder.getSvcord().getCustomerReference().build();
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createIntput = this.entityBuilder.getSvcord()
        .getCreateServicingOrderProducerInput().build();
    createIntput.setCustomerReference(customerReference);

    Svcord27 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createIntput);

    // Payment order 호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD27");
    paymentOrderInput.setExternalSerive("SVCORD27");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
        .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord()
        .getWithdrawalDomainServiceOutput()
        .setTrasactionId(paymentOrderOutput.getTransactionId())
        .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderResult()))
        .build();

    return withdrawalDomainServiceOutput;
  }
}
