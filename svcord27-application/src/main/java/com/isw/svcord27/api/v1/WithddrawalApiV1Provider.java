package com.isw.svcord27.api.v1;

import com.isw.svcord27.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord27.sdk.api.v1.api.WithddrawalApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord27.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord27.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord27.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord27.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord27.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord27.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord27.sdk.domain.svcord.type.ServicingOrderWorkResult;

import okhttp3.ResponseBody;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A stub that provides implementation for the WithddrawalApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord27.sdk.api.v1.api")
public class WithddrawalApiV1Provider implements WithddrawalApiV1Delegate {

  @Autowired
  WithdrawalDomainService withdrawalDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(
      WithdrawalBodySchema withdrawalBodySchema) {

    WithdrawalDomainServiceInput withdrawalDomainServiceInput = entityBuilder.getSvcord()
        .getWithdrawalDomainServiceInput()
        .setAccountNumber(withdrawalBodySchema.getAccountNumber())
        .setAmount(withdrawalBodySchema.getAmount())
        .build();

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = withdrawalDomainService
        .execute(withdrawalDomainServiceInput);

    if (withdrawalDomainServiceOutput == null) {
      ErrorSchema errorSchema = new ErrorSchema();
      errorSchema.setErrorId("500");
      errorSchema.setErrorMsg("INTERNAL ERROR");
      ResponseEntity.status(500).body(errorSchema);
    }

    WithdrawalResponseSchema withdrawalResponse = new WithdrawalResponseSchema();

    withdrawalResponse.setTransactionID(withdrawalDomainServiceOutput.getTrasactionId());
    withdrawalResponse
        .setServicingOrderWorkTaskResult(withdrawalDomainServiceOutput.getServicingOrderWorkResult().toString());

    return ResponseEntity.ok(withdrawalResponse);
  }
}
